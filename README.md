# Overseer Laravel client

This is the Laravel client for [Overseer](https://github.com/fragkp/overseer).

## Requirements

The client works for all Laravel versions since 6.0.

> Why do you support lower versions?   
> All versions below 6.0 are not supported by Laravel anymore.

## Installation

You can install the package via composer:

```bash
composer require fragkp/overseer-laravel-client
```

Publish the configuration file via:

```bash
php artisan vendor:publish --tag=overseer
```

Edit your published configuration to match your settings.

To make sure, all your configurations works correctly, run:

```bash
php artisan overseer:ping
```

## Usage

By default, all of your commands will be synchronized to Overseer.
To change this behavior, set the configuration `sync_all` value to false.

Alternatively, you could also disable or enable single commands.

Example:

```php
$schedule->command(YourCommand::class)
    ->daily()
    ->disableSync();
    // or
    ->enableSync();
```

### Sync your commands

To inform Overseer about your commands, it's necessary to run:

```bash
php artisan overseer:sync
```

> Important: Run this command always when you make changes to your Scheduler.
> A good place would be inside your CI.

## Credits

[Kevin Pohl](https://github.com/fragkp)   
[All Contributors](https://github.com/fragkp/overseer-client/contributors)   
The request class is heavily inspired by Facade's Flare Client PHP package.   
Also a big thank you to Spatie for all open source packages they made.

## License

MIT License (MIT). Please see [License File](LICENSE.md) for more information.
