<?php

namespace Fragkp\OverseerLaravelClient\Tests;

use Mockery;
use Illuminate\Support\Str;
use Fragkp\OverseerClient\Http\Client;
use Illuminate\Console\Scheduling\Event;
use Fragkp\OverseerClient\Listeners\SendStarting;
use Illuminate\Console\Events\ScheduledTaskStarting;
use Fragkp\OverseerClient\Tests\Fakes\FakeEventMutex;

class SendStartingListenerTest extends TestCase
{
    /** @test */
    public function send_client_call()
    {
        $client = $this->mockClient();
        $client->shouldReceive('post')
            ->once()
            ->with('task/started', Mockery::any())
            ->andReturn([]);

        $sendFinished = new SendStarting($client);
        $sendFinished->handle(
            $this->scheduleTaskFinished()
        );
    }

    /** @test */
    public function skip_client_call_when_sync_all_is_disabled()
    {
        config(['overseer.sync_all' => false]);

        $client = $this->mockClient();
        $client->shouldNotReceive('post');

        $sendFinished = new SendStarting($client);
        $sendFinished->handle(
            $this->scheduleTaskFinished()
        );
    }

    /** @test */
    public function send_client_call_when_sync_all_is_disabled_and_task_sync_is_enabled()
    {
        config(['overseer.sync_all' => false]);

        $client = $this->mockClient();
        $client->shouldReceive('post')
            ->once()
            ->with('task/started', Mockery::any())
            ->andReturn([]);

        $sendFinished = new SendStarting($client);
        $sendFinished->handle(
            $this->scheduleTaskFinished(true)
        );
    }

    /** @test */
    public function skip_client_call_when_sync_all_is_disabled_and_task_sync_is_disabled()
    {
        config(['overseer.sync_all' => false]);

        $client = $this->mockClient();
        $client->shouldNotReceive('post');

        $sendFinished = new SendStarting($client);
        $sendFinished->handle(
            $this->scheduleTaskFinished(false)
        );
    }

    /** @test */
    public function send_client_call_when_sync_all_is_enabled()
    {
        config(['overseer.sync_all' => true]);

        $client = $this->mockClient();
        $client->shouldReceive('post')
            ->once()
            ->with('task/started', Mockery::any())
            ->andReturn([]);

        $sendFinished = new SendStarting($client);
        $sendFinished->handle(
            $this->scheduleTaskFinished()
        );
    }

    /** @test */
    public function send_client_call_when_sync_all_is_enabled_and_task_sync_is_enabled()
    {
        config(['overseer.sync_all' => true]);

        $client = $this->mockClient();
        $client->shouldReceive('post')
            ->once()
            ->with('task/started', Mockery::any())
            ->andReturn([]);

        $sendFinished = new SendStarting($client);
        $sendFinished->handle(
            $this->scheduleTaskFinished(true)
        );
    }

    /** @test */
    public function skip_client_call_when_sync_all_is_enabled_and_task_sync_is_disabled()
    {
        config(['overseer.sync_all' => true]);

        $client = $this->mockClient();
        $client->shouldNotReceive('post');

        $sendFinished = new SendStarting($client);
        $sendFinished->handle(
            $this->scheduleTaskFinished(false)
        );
    }

    protected function mockClient()
    {
        return Mockery::mock(new Client('https://fake-url', 'secret-token'));
    }

    protected function scheduleTaskFinished(?bool $syncToOverseer = null)
    {
        $event = new Event(
            new FakeEventMutex,
            'fake-command'
        );

        $event->runUuid = Str::uuid();

        if (! is_null($syncToOverseer)) {
            $event->syncToOverseer = $syncToOverseer;
        }

        return new ScheduledTaskStarting($event, 0);
    }
}
