<?php

namespace Fragkp\OverseerLaravelClient\Tests;

use Ramsey\Uuid\UuidInterface;
use Illuminate\Console\Scheduling\Schedule;
use Fragkp\OverseerClient\Tests\Fakes\FakeConsoleKernel;
use Illuminate\Contracts\Console\Kernel as ConsoleKernelContract;

class AttachUuidsToScheduleEventsTest extends TestCase
{
    protected function resolveApplicationConsoleKernel($app)
    {
        $app->singleton(ConsoleKernelContract::class, FakeConsoleKernel::class);
    }

    /** @test */
    public function attach_uuid()
    {
        $event = app(Schedule::class)->events()[0];

        $this->assertInstanceOf(UuidInterface::class, $event->runUuid);
    }
}
