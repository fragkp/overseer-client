<?php

namespace Fragkp\OverseerLaravelClient\Tests;

use Orchestra\Testbench\TestCase as OrchestraTestCase;
use Fragkp\OverseerClient\OverseerClientServiceProvider;

abstract class TestCase extends OrchestraTestCase
{
    protected function getPackageProviders($app)
    {
        return [
            OverseerClientServiceProvider::class,
        ];
    }

    protected function getEnvironmentSetUp($app)
    {
        $app['config']->set('overseer.url', 'https://fake-url');
    }
}
