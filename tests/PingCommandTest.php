<?php

namespace Fragkp\OverseerLaravelClient\Tests;

use Fragkp\OverseerClient\Http\Client;
use Fragkp\OverseerClient\Http\Response;
use Fragkp\OverseerClient\Console\Commands\PingCommand;

class PingCommandTest extends TestCase
{
    /** @test */
    public function ping_successful()
    {
        $this->mock(Client::class, function ($mock) {
            $mock->shouldReceive('get')
                ->once()
                ->with('ping')
                ->andReturn(new Response([
                    'http_code' => 200,
                ], [], ''));
        });

        $this->artisan(PingCommand::class)
            ->expectsOutput('✅ The schedule monitor is correctly configured.')
            ->assertExitCode(0);
    }
}
