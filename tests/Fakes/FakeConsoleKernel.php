<?php

namespace Fragkp\OverseerLaravelClient\Tests\Fakes;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class FakeConsoleKernel extends ConsoleKernel
{
    protected function schedule(Schedule $schedule)
    {
        $schedule
            ->command('inspire')
            ->everyMinute();
    }
}
