<?php

namespace Fragkp\OverseerLaravelClient\Tests\Fakes;

use Illuminate\Console\Scheduling\Event;
use Illuminate\Console\Scheduling\EventMutex;

class FakeEventMutex implements EventMutex
{
    public function create(Event $event)
    {
        return true;
    }

    public function exists(Event $event)
    {
        return true;
    }

    public function forget(Event $event)
    {
        return;
    }
}
