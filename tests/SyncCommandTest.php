<?php

namespace Fragkp\OverseerLaravelClient\Tests;

use Mockery;
use Illuminate\Console\Application;
use Fragkp\OverseerClient\Http\Client;
use Illuminate\Console\Scheduling\Schedule;
use Fragkp\OverseerClient\Tests\Fakes\FakeJob;
use Fragkp\OverseerClient\Console\Commands\SyncCommand;
use Fragkp\OverseerClient\Tests\Fakes\FakeInvokableCommand;

class SyncCommandTest extends TestCase
{
    /** @test */
    public function sync_successful()
    {
        $this->mock(Client::class, function ($mock) {
            $mock->shouldReceive('post')
                ->once()
                ->with('sync', ['commands' => []])
                ->andReturn([]);
        });

        $this->artisan(SyncCommand::class)
            ->expectsOutput('✅ Synchronized all schedule commands (0).')
            ->assertExitCode(0);
    }

    /** @test */
    public function sync_successful_with_scheduled_command()
    {
        $event = app(Schedule::class)->command('inspire')->hourly();

        $this->mock(Client::class, function ($mock) use ($event) {
            $mock->shouldReceive('post')
                ->once()
                ->with('sync', Mockery::on(function ($parameters) use ($event) {
                    $this->assertEquals($parameters['commands'][0]['php_binary'], Application::phpBinary());
                    $this->assertEquals($parameters['commands'][0]['artisan_binary'], Application::artisanBinary());
                    $this->assertEquals($parameters['commands'][0]['command'], 'inspire');
                    $this->assertEquals($parameters['commands'][0]['description'], null);
                    $this->assertEquals($parameters['commands'][0]['mutex_name'], $event->mutexName());
                    $this->assertEquals($parameters['commands'][0]['expression'], $event->expression);
                    $this->assertEquals($parameters['commands'][0]['timezone'], $event->timezone);
                    $this->assertEquals($parameters['commands'][0]['user'], $event->user);
                    $this->assertEquals($parameters['commands'][0]['environments'], $event->environments);
                    $this->assertEquals($parameters['commands'][0]['even_in_maintenance_mode'], $event->evenInMaintenanceMode);
                    $this->assertEquals($parameters['commands'][0]['without_overlapping'], $event->withoutOverlapping);
                    $this->assertEquals($parameters['commands'][0]['expires_at'], $event->expiresAt);
                    $this->assertEquals($parameters['commands'][0]['on_one_server'], $event->onOneServer);
                    $this->assertEquals($parameters['commands'][0]['output_to'], $event->output);
                    $this->assertEquals($parameters['commands'][0]['run_in_background'], $event->runInBackground);

                    return true;
                }))
                ->andReturn([]);
        });

        $this->artisan(SyncCommand::class)
            ->expectsOutput('✅ Synchronized all schedule commands (1).')
            ->assertExitCode(0);
    }

    /** @test */
    public function sync_successful_with_scheduled_exec()
    {
        $event = app(Schedule::class)->exec('inspire')->hourly();

        $this->mock(Client::class, function ($mock) use ($event) {
            $mock->shouldReceive('post')
                ->once()
                ->with('sync', Mockery::on(function ($parameters) use ($event) {
                    $this->assertEquals($parameters['commands'][0]['php_binary'], Application::phpBinary());
                    $this->assertEquals($parameters['commands'][0]['artisan_binary'], Application::artisanBinary());
                    $this->assertEquals($parameters['commands'][0]['command'], 'inspire');
                    $this->assertEquals($parameters['commands'][0]['description'], null);
                    $this->assertEquals($parameters['commands'][0]['mutex_name'], $event->mutexName());
                    $this->assertEquals($parameters['commands'][0]['expression'], $event->expression);
                    $this->assertEquals($parameters['commands'][0]['timezone'], $event->timezone);
                    $this->assertEquals($parameters['commands'][0]['user'], $event->user);
                    $this->assertEquals($parameters['commands'][0]['environments'], $event->environments);
                    $this->assertEquals($parameters['commands'][0]['even_in_maintenance_mode'], $event->evenInMaintenanceMode);
                    $this->assertEquals($parameters['commands'][0]['without_overlapping'], $event->withoutOverlapping);
                    $this->assertEquals($parameters['commands'][0]['expires_at'], $event->expiresAt);
                    $this->assertEquals($parameters['commands'][0]['on_one_server'], $event->onOneServer);
                    $this->assertEquals($parameters['commands'][0]['output_to'], $event->output);
                    $this->assertEquals($parameters['commands'][0]['run_in_background'], $event->runInBackground);

                    return true;
                }))
                ->andReturn([]);
        });

        $this->artisan(SyncCommand::class)
            ->expectsOutput('✅ Synchronized all schedule commands (1).')
            ->assertExitCode(0);
    }

    /** @test */
    public function sync_successful_with_scheduled_job()
    {
        $event = app(Schedule::class)->job(FakeJob::class)->hourly();

        $this->mock(Client::class, function ($mock) use ($event) {
            $mock->shouldReceive('post')
                ->once()
                ->with('sync', Mockery::on(function ($parameters) use ($event) {
                    $this->assertEquals($parameters['commands'][0]['php_binary'], Application::phpBinary());
                    $this->assertEquals($parameters['commands'][0]['artisan_binary'], Application::artisanBinary());
                    $this->assertEquals($parameters['commands'][0]['command'], 'Closure');
                    $this->assertEquals($parameters['commands'][0]['description'], FakeJob::class);
                    $this->assertEquals($parameters['commands'][0]['mutex_name'], $event->mutexName());
                    $this->assertEquals($parameters['commands'][0]['expression'], $event->expression);
                    $this->assertEquals($parameters['commands'][0]['timezone'], $event->timezone);
                    $this->assertEquals($parameters['commands'][0]['user'], $event->user);
                    $this->assertEquals($parameters['commands'][0]['environments'], $event->environments);
                    $this->assertEquals($parameters['commands'][0]['even_in_maintenance_mode'], $event->evenInMaintenanceMode);
                    $this->assertEquals($parameters['commands'][0]['without_overlapping'], $event->withoutOverlapping);
                    $this->assertEquals($parameters['commands'][0]['expires_at'], $event->expiresAt);
                    $this->assertEquals($parameters['commands'][0]['on_one_server'], $event->onOneServer);
                    $this->assertEquals($parameters['commands'][0]['output_to'], $event->output);
                    $this->assertEquals($parameters['commands'][0]['run_in_background'], $event->runInBackground);

                    return true;
                }))
                ->andReturn([]);
        });

        $this->artisan(SyncCommand::class)
            ->expectsOutput('✅ Synchronized all schedule commands (1).')
            ->assertExitCode(0);
    }

    /** @test */
    public function sync_successful_with_scheduled_call()
    {
        $event = app(Schedule::class)->call(function () {
            return null;
        })->hourly();

        $this->mock(Client::class, function ($mock) use ($event) {
            $mock->shouldReceive('post')
                ->once()
                ->with('sync', Mockery::on(function ($parameters) use ($event) {
                    $this->assertEquals($parameters['commands'][0]['php_binary'], Application::phpBinary());
                    $this->assertEquals($parameters['commands'][0]['artisan_binary'], Application::artisanBinary());
                    $this->assertEquals($parameters['commands'][0]['command'], 'Closure');
                    $this->assertEquals($parameters['commands'][0]['description'], null);
                    $this->assertEquals($parameters['commands'][0]['mutex_name'], $event->mutexName());
                    $this->assertEquals($parameters['commands'][0]['expression'], $event->expression);
                    $this->assertEquals($parameters['commands'][0]['timezone'], $event->timezone);
                    $this->assertEquals($parameters['commands'][0]['user'], $event->user);
                    $this->assertEquals($parameters['commands'][0]['environments'], $event->environments);
                    $this->assertEquals($parameters['commands'][0]['even_in_maintenance_mode'], $event->evenInMaintenanceMode);
                    $this->assertEquals($parameters['commands'][0]['without_overlapping'], $event->withoutOverlapping);
                    $this->assertEquals($parameters['commands'][0]['expires_at'], $event->expiresAt);
                    $this->assertEquals($parameters['commands'][0]['on_one_server'], $event->onOneServer);
                    $this->assertEquals($parameters['commands'][0]['output_to'], $event->output);
                    $this->assertEquals($parameters['commands'][0]['run_in_background'], $event->runInBackground);

                    return true;
                }))
                ->andReturn([]);
        });

        $this->artisan(SyncCommand::class)
            ->expectsOutput('✅ Synchronized all schedule commands (1).')
            ->assertExitCode(0);
    }

    /** @test */
    public function sync_successful_with_scheduled_invoked_class()
    {
        $event = app(Schedule::class)->call(new FakeInvokableCommand)->hourly();

        $this->mock(Client::class, function ($mock) use ($event) {
            $mock->shouldReceive('post')
                ->once()
                ->with('sync', Mockery::on(function ($parameters) use ($event) {
                    $this->assertEquals($parameters['commands'][0]['php_binary'], Application::phpBinary());
                    $this->assertEquals($parameters['commands'][0]['artisan_binary'], Application::artisanBinary());
                    $this->assertEquals($parameters['commands'][0]['command'], 'Closure');
                    $this->assertEquals($parameters['commands'][0]['description'], null);
                    $this->assertEquals($parameters['commands'][0]['mutex_name'], $event->mutexName());
                    $this->assertEquals($parameters['commands'][0]['expression'], $event->expression);
                    $this->assertEquals($parameters['commands'][0]['timezone'], $event->timezone);
                    $this->assertEquals($parameters['commands'][0]['user'], $event->user);
                    $this->assertEquals($parameters['commands'][0]['environments'], $event->environments);
                    $this->assertEquals($parameters['commands'][0]['even_in_maintenance_mode'], $event->evenInMaintenanceMode);
                    $this->assertEquals($parameters['commands'][0]['without_overlapping'], $event->withoutOverlapping);
                    $this->assertEquals($parameters['commands'][0]['expires_at'], $event->expiresAt);
                    $this->assertEquals($parameters['commands'][0]['on_one_server'], $event->onOneServer);
                    $this->assertEquals($parameters['commands'][0]['output_to'], $event->output);
                    $this->assertEquals($parameters['commands'][0]['run_in_background'], $event->runInBackground);

                    return true;
                }))
                ->andReturn([]);
        });

        $this->artisan(SyncCommand::class)
            ->expectsOutput('✅ Synchronized all schedule commands (1).')
            ->assertExitCode(0);
    }

    /** @test */
    public function sync_successful_with_custom_description()
    {
        app(Schedule::class)->exec('inspire')->description('fake description')->hourly();

        $this->mock(Client::class, function ($mock) {
            $mock->shouldReceive('post')
                ->once()
                ->with('sync', Mockery::on(function ($parameters) {
                    $this->assertEquals($parameters['commands'][0]['command'], 'inspire');
                    $this->assertEquals($parameters['commands'][0]['description'], 'fake description');

                    return true;
                }))
                ->andReturn([]);
        });

        $this->artisan(SyncCommand::class)
            ->expectsOutput('✅ Synchronized all schedule commands (1).')
            ->assertExitCode(0);
    }
}
