<?php

namespace Fragkp\OverseerLaravelClient;

use Illuminate\Support\Str;
use Illuminate\Support\ServiceProvider;
use Illuminate\Console\Scheduling\Event;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Contracts\Events\Dispatcher;
use Fragkp\OverseerLaravelClient\Http\Client;
use Illuminate\Console\Events\ScheduledTaskFinished;
use Illuminate\Console\Events\ScheduledTaskStarting;
use Fragkp\OverseerLaravelClient\Listeners\SendStarting;
use Fragkp\OverseerLaravelClient\Listeners\SendFinished;
use Fragkp\OverseerLaravelClient\Console\Commands\PingCommand;
use Fragkp\OverseerLaravelClient\Console\Commands\SyncCommand;

class OverseerClientServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/overseer.php', 'overseer');

        $this->bindApiClient();
    }

    public function boot()
    {
        if (! $this->app->runningInConsole()) {
            return;
        }

        $this->publishes([__DIR__ . '/../config/overseer.php' => config_path('overseer.php')], 'overseer-config');

        $this->registerCommands();

        $this->registerScheduleEventListeners();

        $this->attachUuidsToScheduleEvents();

        $this->registerMacros();
    }

    protected function bindApiClient()
    {
        $this->app->singleton(Client::class, function () {
            return new Client(config('overseer.url'), config('overseer.token'));
        });
    }

    protected function registerCommands()
    {
        $this->commands([
            PingCommand::class,
            SyncCommand::class,
        ]);
    }

    protected function registerScheduleEventListeners()
    {
        $dispatcher = $this->app->make(Dispatcher::class);
        $dispatcher->listen(ScheduledTaskStarting::class, SendStarting::class);
        $dispatcher->listen(ScheduledTaskFinished::class, SendFinished::class);
    }

    protected function attachUuidsToScheduleEvents()
    {
        $this->app->resolving(Schedule::class, function (Schedule $schedule) {
            foreach ($schedule->events() as $event) {
                $event->runUuid = Str::uuid();
            }
        });
    }

    protected function registerMacros()
    {
        Event::macro('disableSync', function () {
            $this->syncToOverseer = false;
        });

        Event::macro('enableSync', function () {
            $this->syncToOverseer = true;
        });
    }
}
