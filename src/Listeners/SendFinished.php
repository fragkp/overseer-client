<?php

namespace Fragkp\OverseerLaravelClient\Listeners;

use Illuminate\Console\Events\ScheduledTaskFinished;

class SendFinished extends SendListener
{
    /**
     * Handle the event.
     *
     * @param \Illuminate\Console\Events\ScheduledTaskFinished $event
     * @return void
     */
    public function handle(ScheduledTaskFinished $event)
    {
        if ($this->shouldNotSendEvent($event)) {
            return;
        }

        $this->client->post('task/finished', [
            'mutex_name' => $event->task->mutexName(),
            'uuid'       => $event->task->runUuid->toString(),
            'exit_code'  => $event->task->exitCode ?? 0,
            'runtime'    => $event->runtime,
            'output'     => file_exists($event->task->output)
                ? file_get_contents($event->task->output)
                : null
        ]);
    }
}
