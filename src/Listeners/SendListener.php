<?php

namespace Fragkp\OverseerLaravelClient\Listeners;

use Fragkp\OverseerLaravelClient\Http\Client;

abstract class SendListener
{
    /**
     * @var \Fragkp\OverseerLaravelClient\Http\Client
     */
    protected $client;

    /**
     * Create the event listener.
     *
     * @param \Fragkp\OverseerLaravelClient\Http\Client $client
     * @return void
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @param \Illuminate\Console\Events\ScheduledTaskStarting|\Illuminate\Console\Events\ScheduledTaskFinished $event
     * @return bool
     */
    protected function shouldSendEvent($event)
    {
        if (config('overseer.sync_all') === true && ($event->task->syncToOverseer ?? true) === false) {
            return false;
        }

        if (config('overseer.sync_all') === false && ($event->task->syncToOverseer ?? false) === false) {
            return false;
        }

        return true;
    }

    /**
     * @param \Illuminate\Console\Events\ScheduledTaskStarting|\Illuminate\Console\Events\ScheduledTaskFinished $event
     * @return bool
     */
    protected function shouldNotSendEvent($event)
    {
        return ! $this->shouldSendEvent($event);
    }
}
