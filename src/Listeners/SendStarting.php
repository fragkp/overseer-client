<?php

namespace Fragkp\OverseerLaravelClient\Listeners;

use Illuminate\Console\Events\ScheduledTaskStarting;

class SendStarting extends SendListener
{
    /**
     * Handle the event.
     *
     * @param \Illuminate\Console\Events\ScheduledTaskStarting $event
     * @return void
     */
    public function handle(ScheduledTaskStarting $event)
    {
        if ($this->shouldNotSendEvent($event)) {
            return;
        }

        $event->task->storeOutput();

        $this->client->post('task/started', [
            'mutex_name'  => $event->task->mutexName(),
            'uuid'        => $event->task->runUuid->toString(),
            'environment' => app()->environment(),
        ]);
    }
}
