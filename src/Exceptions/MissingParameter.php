<?php

namespace Fragkp\OverseerLaravelClient\Exceptions;

use Exception;

class MissingParameter extends Exception
{
    public static function create(string $name)
    {
        return new static("Parameter \"{$name}\" is required.");
    }
}
