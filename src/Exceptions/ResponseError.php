<?php

namespace Fragkp\OverseerLaravelClient\Exceptions;

use Exception;
use Fragkp\OverseerLaravelClient\Http\Response;

class ResponseError extends Exception
{
    /**
     * @var \Fragkp\OverseerLaravelClient\Http\Response
     */
    public $response;

    /**
     * @param \Fragkp\OverseerLaravelClient\Http\Response $response
     * @return static
     */
    public static function createResponseException(Response $response)
    {
        return tap(new static(static::getErrorFromResponse($response)), function ($exception) use ($response) {
            $exception->response = $response;
        });
    }

    /**
     * @param \Fragkp\OverseerLaravelClient\Http\Response $response
     * @return string
     */
    protected static function getErrorFromResponse(Response $response)
    {
        if ($response->hasBody() && isset($response->getBody()['message'])) {
            return $response->getBody()['message'];
        }

        return $response->getError();
    }
}
