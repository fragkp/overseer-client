<?php

namespace Fragkp\OverseerLaravelClient\Http;

use Fragkp\OverseerLaravelClient\Exceptions\MissingParameter;

class Client
{
    /**
     * @var string
     */
    protected $url;

    /**
     * @var string
     */
    protected $token;

    /**
     * @param string|null $url
     * @param string|null $token
     */
    public function __construct(?string $url, ?string $token)
    {
        if (empty($url)) {
            throw MissingParameter::create('url');
        }

        if (empty($token)) {
            throw MissingParameter::create('token');
        }

        $this->url   = $url;
        $this->token = $token;
    }

    /**
     * @param string $url
     * @param array  $arguments
     * @return \Fragkp\OverseerLaravelClient\Http\Response
     */
    public function get(string $url, array $arguments = [])
    {
        return $this->makeRequest('get', $url, $arguments);
    }

    /**
     * @param string $url
     * @param array  $arguments
     * @return \Fragkp\OverseerLaravelClient\Http\Response
     */
    public function post(string $url, array $arguments = [])
    {
        return $this->makeRequest('post', $url, $arguments);
    }

    /**
     * @param string $method
     * @param string $url
     * @param array  $data
     * @return \Fragkp\OverseerLaravelClient\Http\Response
     */
    protected function makeRequest(string $method, string $url, array $data = [])
    {
        return $this->makeCurlRequest($method, "{$this->url}/{$url}", $data);
    }

    /**
     * @param string $method
     * @param string $url
     * @param array  $data
     * @return \Fragkp\OverseerLaravelClient\Http\Response
     */
    protected function makeCurlRequest(string $method, string $url, array $data = [])
    {
        $curlHandle = $this->getCurlHandle($url);

        switch ($method) {
            case 'get':
                $parameters = empty($data) ? '' : '?' . http_build_query($data);

                curl_setopt($curlHandle, CURLOPT_URL, $url . $parameters);

                break;

            case 'post':
                curl_setopt($curlHandle, CURLOPT_POST, true);
                curl_setopt($curlHandle, CURLOPT_POSTFIELDS, json_encode($data));

                break;
        }

        return new Response(
            json_decode(curl_exec($curlHandle), true) ?: [],
            curl_getinfo($curlHandle),
            curl_error($curlHandle)
        );
    }

    /**
     * @param string $url
     * @return resource
     */
    protected function getCurlHandle(string $url)
    {
        $curlHandle = curl_init();

        curl_setopt($curlHandle, CURLOPT_URL, $url);
        curl_setopt($curlHandle, CURLOPT_HTTPHEADER, [
            'Accept: application/json',
            'Content-Type: application/json',
            "X-API-TOKEN: {$this->token}",
        ]);
        curl_setopt($curlHandle, CURLOPT_USERAGENT, 'Overseer API');
        curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curlHandle, CURLOPT_TIMEOUT, 10);
        curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($curlHandle, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
        curl_setopt($curlHandle, CURLOPT_ENCODING, '');
        curl_setopt($curlHandle, CURLINFO_HEADER_OUT, true);

        return $curlHandle;
    }
}
