<?php

namespace Fragkp\OverseerLaravelClient\Http;

class Response
{
    /**
     * @var array
     */
    protected $headers;

    /**
     * @var array
     */
    protected $body;

    /**
     * @var string
     */
    protected $error;

    /**
     * @param array  $body
     * @param array  $headers
     * @param string $error
     */
    public function __construct(array $body, array $headers, string $error)
    {
        $this->body    = $body;
        $this->headers = $headers;
        $this->error   = $error;
    }

    /**
     * @return array
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * @return array
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @return bool
     */
    public function hasBody()
    {
        return $this->body != false;
    }

    /**
     * @return string
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * @return int
     */
    public function getHttpResponseCode()
    {
        return (int) $this->headers['http_code'];
    }
}
