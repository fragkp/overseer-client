<?php

namespace Fragkp\OverseerLaravelClient\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Console\Application;
use Illuminate\Console\Scheduling\Event;
use Illuminate\Console\Scheduling\Schedule;
use Fragkp\OverseerLaravelClient\Http\Client;
use Illuminate\Console\Scheduling\CallbackEvent;

class SyncCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'overseer:sync';

    /**
     * Execute the console command.
     *
     * @param \Illuminate\Console\Scheduling\Schedule   $schedule
     * @param \Fragkp\OverseerLaravelClient\Http\Client $client
     * @return void
     */
    public function handle(Schedule $schedule, Client $client)
    {
        $commands = $this->mapEvents($schedule->events());

        $client->post('sync', ['commands' => $commands]);

        $this->info('✅ Synchronized all schedule commands (' . count($commands) . ').');
    }

    /**
     * @param \Illuminate\Console\Scheduling\Event[] $events
     * @return array
     */
    protected function mapEvents(array $events)
    {
        return array_map(function (Event $event) {
            return [
                'php_binary'               => $phpBinary = Application::phpBinary(),
                'artisan_binary'           => $artisanBinary = Application::artisanBinary(),
                'command'                  => $event instanceof CallbackEvent
                    ? 'Closure'
                    : ltrim(str_replace([$phpBinary, $artisanBinary], ['', ''], $event->command)),
                'description'              => $event->description,
                'mutex_name'               => $event->mutexName(),
                'expression'               => $event->expression,
                'timezone'                 => $event->timezone,
                'user'                     => $event->user,
                'environments'             => $event->environments,
                'even_in_maintenance_mode' => $event->evenInMaintenanceMode,
                'without_overlapping'      => $event->withoutOverlapping,
                'expires_at'               => $event->expiresAt,
                'on_one_server'            => $event->onOneServer,
                'output_to'                => $event->output,
                'run_in_background'        => $event->runInBackground,
            ];
        }, $events);
    }
}
