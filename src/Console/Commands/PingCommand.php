<?php

namespace Fragkp\OverseerLaravelClient\Console\Commands;

use Illuminate\Console\Command;
use Fragkp\OverseerLaravelClient\Http\Client;
use Fragkp\OverseerLaravelClient\Exceptions\ResponseError;

class PingCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'overseer:ping';

    /**
     * Execute the console command.
     *
     * @param \Fragkp\OverseerLaravelClient\Http\Client $client
     * @return void
     */
    public function handle(Client $client)
    {
        $response = $client->get('ping');

        if ($response->getHttpResponseCode() !== 200) {
            throw ResponseError::createResponseException($response);
        }

        $this->info('✅ The schedule monitor is correctly configured.');
    }
}
