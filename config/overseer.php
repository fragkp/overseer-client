<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Overseer API URL
    |--------------------------------------------------------------------------
    | Your Overseer API URL. Each project has an unique id at the end.
    |
    */

    'url' => env('OVERSEER_URL', 'https://YOUR-OVERSEER.com/api/PROJECT_ID'),

    /*
    |--------------------------------------------------------------------------
    | Overseer API token
    |--------------------------------------------------------------------------
    | Your Overseer API token. This token authorizes your API calls.
    |
    */

    'token' => env('OVERSEER_TOKEN', 'YOUR-TOKEN'),

    /*
    |--------------------------------------------------------------------------
    | Synchronize all commands
    |--------------------------------------------------------------------------
    | Determine if all command should be synchronized to Overseer.
    |
    */

    'sync_all' => env('OVERSEER_SYNC_ALL', true),

];
